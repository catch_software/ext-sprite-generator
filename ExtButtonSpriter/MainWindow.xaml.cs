﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExtButtonSpriter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var c = chopper.canvas;
            RenderTargetBitmap bmp = new RenderTargetBitmap((int)c.ActualWidth, (int)c.ActualHeight, 96, 96, PixelFormats.Pbgra32);

            c.Measure(c.RenderSize); //Important
            c.Arrange(new Rect(c.RenderSize)); //Important

            bmp.Render(c);


            SavePng(bmp);
        }

        static void SaveGif(RenderTargetBitmap bmp)
        {
            var enc = new GifBitmapEncoder()
                {
                    Palette = new BitmapPalette(bmp, 256),
                    Frames =
                        {
                            BitmapFrame.Create(bmp)
                        }
                };
            using (var fileStream = File.OpenWrite("output.gif"))
            {
                enc.Save(fileStream);
                fileStream.Flush(true);
            }
        }
        static void SavePng(RenderTargetBitmap bmp)
        {
            var enc = new PngBitmapEncoder()
                {
                    Frames =
                        {
                            BitmapFrame.Create(bmp)
                        }
                };
            using (var fileStream = File.OpenWrite("output.png"))
            {
                enc.Save(fileStream);
                fileStream.Flush(true);
            }
        }
    }
}
