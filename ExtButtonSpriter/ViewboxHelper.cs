﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ExtButtonSpriter
{
    public class ViewboxHelper
    {


        public static NineGridCell GetCell(VisualBrush obj)
        {
            return (NineGridCell)obj.GetValue(CellProperty);
        }

        public static void SetCell(VisualBrush obj, NineGridCell value)
        {
            obj.SetValue(CellProperty, value);
        }

        // Using a DependencyProperty as the backing store for Cell.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CellProperty = DependencyProperty.RegisterAttached("Cell",
            typeof(NineGridCell),
            typeof(ViewboxHelper),
            new PropertyMetadata(NineGridCell.None, (o, a) => Update((VisualBrush)o, (NineGridCell)a.NewValue)));

        static void Update(VisualBrush visualBrush, NineGridCell nineGridCell)
        {
            if (nineGridCell == NineGridCell.None)
            {
                return;
            }

            visualBrush.ViewboxUnits = BrushMappingMode.Absolute;

            var descriptor = DependencyPropertyDescriptor.FromProperty(VisualBrush.VisualProperty, typeof(VisualBrush));
            descriptor.AddValueChanged(visualBrush, (sender, args) =>
                {
                    var image = visualBrush.Visual as Image;
                    if (image != null)
                    {
                        UpdateViewbox(visualBrush, image.ActualWidth, image.ActualHeight, nineGridCell);
                        image.Loaded += (o, eventArgs) => UpdateViewbox(visualBrush, image.ActualWidth, image.ActualHeight, nineGridCell);
                    }
                });

        }

        static void UpdateViewbox(VisualBrush visualBrush, double w, double h, NineGridCell cell)
        {
            if (w == 0 || h == 0)
            {
                return;
            }
            Rect rect = GetRect(w, h, cell);
            visualBrush.Viewbox = rect;
        }

        static Rect GetRect(double w, double h, NineGridCell cell)
        {
            const int thickness = 3;
            const int bodyHeight = 20;

            var outerRect = new Rect(0, 0, w, h);
            var innerRect = Rect.Inflate(outerRect, -thickness, -thickness);

            if (cell == NineGridCell.Center)
            {
                innerRect.Width = 1;
                innerRect.Height = bodyHeight;
                return innerRect;
            }

            var prop = typeof(Rect).GetProperty(cell.ToString());
            var oOuter = prop.GetValue(outerRect);
            

            if (oOuter is Point)
            {
                var outer = (Point)oOuter;
                var inner = (Point)prop.GetValue(innerRect);
                return new Rect(outer, inner);
            }
            else
            {
                var outer = (double)oOuter;
                var inner = (double)prop.GetValue(innerRect);
                Point po = new Point(outer, outer);
                Point pi = new Point(inner, inner);
                if (cell == NineGridCell.Left || cell == NineGridCell.Right)
                {
                    po.Y = thickness + 1;
                    pi.Y = thickness + 1 + bodyHeight;
                }
                else
                {
                    po.X = thickness + 1;
                    pi.X = thickness + 2;
                }
                return new Rect(po, pi);
            }
        }
    }

    public enum NineGridCell
    {
        None, Center,
        TopLeft, Top, TopRight, Right, BottomRight, Bottom, BottomLeft, Left
    }
}
